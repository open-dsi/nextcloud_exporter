from nextcloud_exporter.database import engine, FileCache, MimeTypes
from .Metric import Metric

from prometheus_client import Gauge
from sqlalchemy import select, func, case
from sqlalchemy.orm import Session


class Files(Metric):
    files_count = Gauge("nextcloud_file_count", "Nextcloud files count", labelnames=["type", "mimetype"])
    files_size = Gauge("nextcloud_file_size", "Nextcloud files size", unit="bytes", labelnames=["type", "mimetype"])
    types_mapping = {
        "files": r"^(files|__groupfolders/[0-9]+)/",
        "trashbin": r"^(files_trashbin|__groupfolders/trash)/",
        "versions": r"^(files_versions|__groupfolders/versions)/"
    }

    def fetch(self):
        with Session(engine) as session:
            s = select(case(
                *[(FileCache.path.regexp_match(re), name) for name, re in self.types_mapping.items()],
                else_="unknown"
            ).label("type"), MimeTypes.mimetype, func.count(FileCache.fileid), func.sum(FileCache.size))\
                .join(FileCache.mimetype_r)\
                .group_by("type", MimeTypes.mimetype)
            for file_type, file_mimetype, file_count, file_size in session.execute(s):
                self.files_count.labels(type=file_type, mimetype=file_mimetype).set(file_count)
                self.files_size.labels(type=file_type, mimetype=file_mimetype).set(file_size)
