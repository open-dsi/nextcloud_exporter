from nextcloud_exporter.database import engine, Share
from .Metric import Metric

from prometheus_client import Gauge
from sqlalchemy import select, func
from sqlalchemy.orm import Session


class Shares(Metric):
    types_mapping = {0: "user", 1: "group", 3: "link", 4: "email", 6: "federated", 7: "circle", 8: "guest",
                     9: "federated_group", 10: "talk", 12: "deck"}
    unsafe_types = [3, 4]
    shares = Gauge("nextcloud_share", "Nextcloud shares", labelnames=["type"])
    unsafe_shares = Gauge("nextcloud_unsafe_share", "Nextcloud unsafe shares", labelnames=["type"])

    def fetch(self):
        with Session(engine) as session:
            s = select(Share.share_type, func.count(Share.id))\
                .group_by(Share.share_type)
            for share_type, share_count in session.execute(s):
                if share_type in self.types_mapping:
                    self.shares.labels(type=self.types_mapping[share_type]).set(share_count)

            s = select(Share.share_type, func.count(Share.id))\
                .where(Share.password == None)\
                .group_by(Share.share_type)
            for share_type, share_count in session.execute(s):
                if share_type in self.unsafe_types:
                    self.unsafe_shares.labels(type=self.types_mapping[share_type]).set(share_count)
