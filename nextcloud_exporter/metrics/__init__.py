from apscheduler.schedulers.background import BlockingScheduler

metrics_scheduler = BlockingScheduler()

from .UserQuota import UserQuota
from .Users import Users
from .UserActivity import UserActivity
from .Shares import Shares
from .Files import Files

metrics = [UserQuota, Users, UserActivity, Shares, Files]
