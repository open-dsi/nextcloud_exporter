from nextcloud_exporter.database import engine, Accounts, Preferences
from .Metric import Metric

from prometheus_client import Gauge
from sqlalchemy import select, func
from sqlalchemy.orm import Session


class Users(Metric):
    user_count = Gauge("nextcloud_user_count", "Number of Nextcloud users")

    def fetch(self):
        with Session(engine) as session:
            users = select(func.count(Preferences.userid))\
                    .where(Preferences.appid == "login")\
                    .where(Preferences.configkey == "lastLogin")
            self.user_count.set(session.execute(users).fetchone()[0])
