from nextcloud_exporter.database import engine, Accounts, Storages, FileCache, MimeTypes
from .Metric import Metric

from prometheus_client import Gauge
from sqlalchemy import select, func
from sqlalchemy.orm import Session


class UserQuota(Metric):
    user_quota = Gauge("nextcloud_user_quota_max", "Nextcloud user quota", unit="bytes", labelnames=["uid"])
    user_quota_used = Gauge("nextcloud_user_quota_used", "Nextcloud user quota", unit="bytes", labelnames=["uid"])

    def fetch(self):
        with Session(engine) as session:
            s = select(Accounts, FileCache.size)\
                .join(Accounts.storages)\
                .join(Storages.files)\
                .join(FileCache.mimetype_r)\
                .where(MimeTypes.mimetype != "httpd/unix-directory")
            for account, used in session.execute(s):
                self.user_quota.labels(uid=account.uid).set(account.get_quota(session))
                self.user_quota_used.labels(uid=account.uid).set(used)
