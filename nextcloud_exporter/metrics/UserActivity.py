from datetime import datetime, timedelta

from nextcloud_exporter.database import engine, AuthToken
from .Metric import Metric

from prometheus_client import Gauge
from sqlalchemy import select, func, distinct
from sqlalchemy.orm import Session


class UserActivity(Metric):
    user_activity = Gauge("nextcloud_user_activity", "Number of Nextcloud user active during the last 15 minutes")

    def fetch(self):
        with Session(engine) as session:
            since = (datetime.now() - timedelta(seconds=self.interval)).timestamp()
            s = select(func.count(distinct(AuthToken.uid))) \
                .where(AuthToken.last_activity >= since)
            self.user_activity.set(session.execute(s).fetchone()[0])
