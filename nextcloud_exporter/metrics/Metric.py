from apscheduler.schedulers.background import BlockingScheduler


class Metric:
    def __init__(self, interval: int = 5, misfire_grace_time: int = None):
        self.interval = interval
        self.misfire_grace_time = misfire_grace_time

    def fetch(self):
        pass

    def schedule(self, scheduler: BlockingScheduler):
        scheduler.add_job(self.fetch, "interval", seconds=self.interval, misfire_grace_time=self.misfire_grace_time)
