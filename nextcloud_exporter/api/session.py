from urllib.parse import urljoin

from requests import Session
from requests.auth import HTTPBasicAuth
from requests.structures import CaseInsensitiveDict
from requests.utils import default_headers


class NextcloudAPI(Session):
    def __init__(self, base_url: str, username: str, password: str):
        super().__init__()
        self.headers = self.default_headers()
        self.api_url = "/ocs/v1.php/"
        self.base_url = urljoin(base_url, self.api_url)
        self.auth = HTTPBasicAuth(username, password)
        if not self.check():
            raise Exception("Failed to connect to Nextcloud API")

    @staticmethod
    def default_headers():
        headers = default_headers()
        headers.update({
            "OCS-APIRequest": True,
            "Accept": "application/json"
        })
        return headers

    def request(self, method, url, *args, **kwargs):
        joined_url = urljoin(self.base_url, url)
        return super().request(method, joined_url, *args, **kwargs)

    def check(self):
        r = self.get("/cloud/capabilities")
        status = r.json().get("ocs", {}).get("meta", {}).get("status", {})
        return status.lower() == "ok"
