from os import environ
from .session import NextcloudAPI

session = NextcloudAPI(base_url=environ.get("NEXTCLOUD_URL"),
                       username=environ.get("NEXTCLOUD_USERNAME"),
                       password=environ.get("NEXTCLOUD_PASSWORD"))
