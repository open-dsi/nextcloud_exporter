from sqlalchemy.orm import relationship

from nextcloud_exporter.database import engine, db_prefix, Base
from sqlalchemy import Table


class Storages(Base):
    __table__ = Table(f"{db_prefix}storages", Base.metadata, autoload_with=engine, extend_existing=True)

    files = relationship("FileCache", primaryjoin="foreign(Storages.numeric_id) == remote(FileCache.storage)", uselist=True)
