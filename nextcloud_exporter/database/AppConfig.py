from nextcloud_exporter.database import engine, db_prefix, Base
from sqlalchemy import Table


class AppConfig(Base):
    __table__ = Table(f"{db_prefix}appconfig", Base.metadata, autoload_with=engine, extend_existing=True)
