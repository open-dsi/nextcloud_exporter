from sqlalchemy.orm import relationship

from nextcloud_exporter.database import engine, db_prefix, Base
from sqlalchemy import Table


class Share(Base):
    __table__ = Table(f"{db_prefix}share", Base.metadata, autoload_with=engine, extend_existing=True)

    files = relationship("FileCache", primaryjoin="foreign(Share.file_source) == remote(FileCache.storage)",
                         uselist=True)
