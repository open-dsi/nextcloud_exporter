from os import environ
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base


db_prefix = environ.get("DB_PREFIX")
engine = create_engine(environ.get("DB_URL"))
Base = declarative_base()

from .Accounts import Accounts
from .Storages import Storages
from .FileCache import FileCache
from .Preferences import Preferences
from .AppConfig import AppConfig
from .Activity import Activity
from .AuthToken import AuthToken
from .Share import Share
from .MimeTypes import MimeTypes
