from sqlalchemy.orm import relationship

from nextcloud_exporter.database import engine, db_prefix, Base
from .Preferences import Preferences
from .AppConfig import AppConfig
from sqlalchemy import Table, select

from nextcloud_exporter.utils import parse_size


class Accounts(Base):
    __table__ = Table(f"{db_prefix}accounts", Base.metadata, autoload_with=engine, extend_existing=True)

    storages = relationship("Storages", primaryjoin="foreign(Storages.id).iendswith(Accounts.uid)", viewonly=True)
    preferences = relationship("Preferences", primaryjoin="foreign(Preferences.userid) == Accounts.uid", uselist=False)
    activities = relationship("Activity", primaryjoin="foreign(Activity.user) == Accounts.uid", uselist=True)

    def get_quota(self, session) -> int:
        quota = session.execute(select(Preferences.configvalue).join(Accounts.preferences).where(Accounts.uid == self.uid).where(Preferences.appid == "files").where(Preferences.configkey == "quota")).one_or_none()

        # If the user doesn't have a quota, it's equivalent to the default one
        if not quota or quota[0] == 'default':
            # Query default quota from applications configurations
            quota = session.execute(select(AppConfig.configvalue).where(AppConfig.appid == "files").where(AppConfig.configkey == "default_quota")).one_or_none()

            # If no default quota set, it's unlimited (none)
            if not quota:
                quota = "none"
            else:
                quota = quota[0]
        else:
            quota = quota[0]

        if quota == "none":
            return 0

        return parse_size(quota)
