from sqlalchemy.orm import relationship

from nextcloud_exporter.database import engine, db_prefix, Base
from sqlalchemy import Table


class FileCache(Base):
    __table__ = Table(f"{db_prefix}filecache", Base.metadata, autoload_with=engine, extend_existing=True)

    mimetype_r = relationship("MimeTypes", primaryjoin="foreign(MimeTypes.id) == FileCache.mimetype", uselist=False)
