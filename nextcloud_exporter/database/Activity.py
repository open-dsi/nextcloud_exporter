from nextcloud_exporter.database import engine, db_prefix, Base
from sqlalchemy import Table


class Activity(Base):
    __table__ = Table(f"{db_prefix}activity", Base.metadata, autoload_with=engine, extend_existing=True)
