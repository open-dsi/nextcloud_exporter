from nextcloud_exporter.utils import parse_size


def test_parse_size():
    assert parse_size("500 MB") == 524288000
