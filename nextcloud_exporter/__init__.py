from os import environ
from logging import getLogger, basicConfig

level = environ.get("LOGLEVEL", "ERROR").upper()

basicConfig(level=level)
getLogger('sqlalchemy.engine').setLevel(level)
