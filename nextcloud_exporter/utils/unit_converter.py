from re import compile as re_compile


number_unit_re = re_compile(r"^([0-9]+)\s*([A-z]+)?$")
units = {
    'b': 1,
    'k': 1024,
    'kb': 1024,
    'mb': 1024 * 1024,
    'm': 1024 * 1024,
    'gb': 1024 * 1024 * 1024,
    'g': 1024 * 1024 * 1024,
    'tb': 1024 * 1024 * 1024 * 1024,
    't': 1024 * 1024 * 1024 * 1024,
    'pb': 1024 * 1024 * 1024 * 1024 * 1024,
    'p': 1024 * 1024 * 1024 * 1024 * 1024,
}



def parse_size(size: str) -> int:
    number, unit = number_unit_re.fullmatch(size).groups()
    unit = unit.lower()
    return round(float(number)*units[unit])
