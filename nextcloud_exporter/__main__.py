from os import environ
from logging import getLogger

from prometheus_client import start_http_server
from nextcloud_exporter.metrics import metrics_scheduler, metrics


logger = getLogger(__name__)

logger.info("Start on port 8000")
start_http_server(int(environ.get("HTTP_PORT", 8000)), environ.get("HTTP_ADDRESS", "0.0.0.0"))

logger.info("Initialising schedule")
for metric in metrics:
    logger.info(metric.__name__)
    m = metric()
    logger.info("First run")
    m.fetch()
    logger.info("Adding to schedule")
    m.schedule(metrics_scheduler)

logger.info("Launching scheduler")
metrics_scheduler.start()
