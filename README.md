# Nextcloud exporter

## Installation
```bash
apt install -y python3-pip
pip install Nextcloud-exporter --extra-index-url https://git.open-dsi.fr/api/v4/projects/649/packages/pypi/simple
useradd -r nextcloud_exporter
```

Install a database driver with apt :
* PostgreSQL : `python3-psycopg2`
* MariaDB : `python3-mysqldb`
* SQLite : Shipped with Python3

Create SystemD service :

`/etc/systemd/system/nextcloud_exporter.service` :
```
[Unit]
Description=Nextcloud exporter
After=syslog.target
After=network.target
Documentation=https://git.open-dsi.fr/nextcloud-extension/nextcloud_exporter

[Service]
ExecStart=/usr/bin/python3 -m nextcloud_exporter
EnvironmentFile=/etc/default/nextcloud_exporter
User=nextcloud_exporter
Type=simple
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=nextcloud_exporter
SuccessExitStatus=143
TimeoutStopSec=120
Restart=always

[Install]
WantedBy=multi-user.target
```

Create configuration in `/etc/default/nextcloud_exporter` :
```dotenv
DB_URL="sqlite://"
DB_PREFIX=oc_
[...]
```

Apply, enable and start new service :
```bash
systemctl daemon-reload
systemctl enable --now nextcloud_exporter
```

## Configuration
Environment variables :

| Name            | Description                                                                                              | Example of variable                                        |
|-----------------|----------------------------------------------------------------------------------------------------------|------------------------------------------------------------|
| `DB_URL`        | The [SQLAlchemy database URL](https://docs.sqlalchemy.org/en/20/core/engines.html#backend-specific-urls) | `postgresql+psycopg2://user:passwd@localhost:5432/db_name` |
| `DB_PREFIX`     | The Nextcloud Database prefix (usually `oc_`)                                                            | `oc_`                                                      |
