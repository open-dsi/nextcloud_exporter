from setuptools import setup, find_packages

setup(
    name="Nextcloud exporter",
    description="A Prometheus Nextcloud exporter",
    url="https://git.open-dsi.fr/nextcloud-extension/nextcloud_exporter",
    author="Florian Charlaix",
    author_email="fcharlaix@open-dsi.fr",
    license="GPLv3+",
    packages=find_packages(),
    setuptools_git_versioning={
        "enabled": True,
        "version_file": "VERSION",
        "count_commits_from_version_file": True,
        "dev_template": "{tag}.dev{ccount}",
        "dirty_template": "{tag}.{branch}{ccount}",
    },
    setup_requires=["setuptools-git-versioning<2"],
    install_requires=[
        "prometheus-client==0.16.0",
        "requests==2.28.2",
        "apscheduler==3.10.1",
        "SQLAlchemy==2.0.5.post1"
    ],
    classifiers=[
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3.9",
    ],
)
